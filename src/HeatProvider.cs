﻿using CodeHatch.Engine.Networking;
using uMod.Common;
using uMod.Common.Command;
using uMod.Text;
using uMod.Utilities;

namespace uMod.Game.Heat
{
    /// <summary>
    /// Provides Universal functionality for the game server
    /// </summary>
    public class HeatProvider : IUniversalProvider
    {
        /// <summary>
        /// Gets the name of the game for which this provider provides
        /// </summary>
        public string GameName => "Heat";

        /// <summary>
        /// Gets the Steam app ID of the game's client, if available
        /// </summary>
        public uint ClientAppId => 656240;

        /// <summary>
        /// Gets the Steam app ID of the game's server, if available
        /// </summary>
        public uint ServerAppId => 996600;

        /// <summary>
        /// Gets a container with important game-specific types
        /// </summary>
        public IGameTypes Types { get; private set; }

        /// <summary>
        /// Gets the singleton instance of this provider
        /// </summary>
        internal static HeatProvider Instance { get; private set; }

        public HeatProvider()
        {
            Instance = this;
        }

        /// <summary>
        /// Gets the command system provider
        /// </summary>
        public HeatCommands CommandSystem { get; private set; }

        /// <summary>
        /// Creates the game-specific command system provider object
        /// </summary>
        /// <param name="commandHandler"></param>
        /// <returns></returns>
        public ICommandSystem CreateCommandSystemProvider(ICommandHandler commandHandler) => CommandSystem = new HeatCommands(commandHandler);

        /// <summary>
        /// Creates the game specific player manager
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        /// <returns></returns>
        public IPlayerManager CreatePlayerManager(IApplication application, ILogger logger)
        {
            return new HeatPlayerManager(application, logger);
        }

        /// <summary>
        /// Creates the game-specific server object
        /// </summary>
        /// <returns></returns>
        public IServer CreateServer()
        {
            IServer server = new HeatServer();
            Types = new GameTypes(server)
            {
                Player = typeof(Player)
            };
            return server;
        }

        /// <summary>
        /// Formats the text with universal markup as game-specific markup
        /// </summary>
        /// <param name="text">text to format</param>
        /// <returns>formatted text</returns>
        public string FormatText(string text) => Formatter.ToRoKAnd7DTD(text);
    }
}
