﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.IOnPlayerChat(CodeHatch.Engine.Chat.PlayerMessageEvent)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.IOnPlayerCommand(CodeHatch.Engine.Chat.PlayerCommandEvent)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.IOnPlayerConnect(CodeHatch.Engine.Networking.Player)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.IOnPlayerConnected(CodeHatch.Networking.Events.Players.PlayerJoinEvent)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.IOnPlayerDisconnected(CodeHatch.Networking.Events.Players.PlayerLeaveEvent)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.IOnServerCommand(CodeHatch.Engine.Core.Commands.CommandInfo)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.OnPlayerBanned(CodeHatch.Networking.Events.Players.PlayerKickEvent)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.OnPlayerKicked(CodeHatch.Networking.Events.Players.PlayerKickEvent)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.OnPlayerRespawn(CodeHatch.Networking.Events.Players.PlayerRespawnEvent)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.OnPlayerSpawn(CodeHatch.Networking.Events.Players.PlayerFirstSpawnEvent)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.OnPlayerSpawned(CodeHatch.Networking.Events.Players.PlayerSpawnEvent)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.Init")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.OnPluginLoaded(uMod.Plugins.Plugin)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.OnServerInitialized")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.OnServerSave")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.Heat.OnServerShutdown")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.HeatConsolePlayer.Ban(System.String,System.TimeSpan)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.HeatConsolePlayer.Kick(System.String)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.HeatConsolePlayer.Heal(System.Single)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.HeatConsolePlayer.Hurt(System.Single)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.HeatConsolePlayer.Rename(System.String)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.HeatConsolePlayer.Respawn(uMod.Common.Position)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.HeatConsolePlayer.Teleport(System.Single,System.Single,System.Single)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.Heat.HeatConsolePlayer.Teleport(uMod.Common.Position)")]
