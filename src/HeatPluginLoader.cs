﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.Heat
{
    /// <summary>
    /// Responsible for loading the core Heat plugin
    /// </summary>
    public class HeatPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(Heat) };

        public HeatPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}
