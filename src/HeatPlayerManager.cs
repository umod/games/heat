﻿using uMod.Auth;
using uMod.Common;

namespace uMod.Game.Heat
{
    /// <summary>
    /// Represents a Heat player manager
    /// </summary>
    public class HeatPlayerManager : PlayerManager<HeatPlayer>
    {
        /// <summary>
        /// Create a new instance of the FortressCraftPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public HeatPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }
    }
}
