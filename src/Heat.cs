using CodeHatch.Common;
using CodeHatch.Engine.Chat;
using CodeHatch.Engine.Core.Commands;
using CodeHatch.Engine.Networking;
using CodeHatch.Networking.Events.Players;
using System;
using System.Linq;
using uMod.Common;
using uMod.Configuration;
using uMod.Plugins;
using uMod.Plugins.Decorators;
using CommandInfo = CodeHatch.Engine.Core.Commands.CommandInfo;

namespace uMod.Game.Heat
{
    /// <summary>
    /// The core Heat plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class Heat : Plugin
    {
        #region Initialization

        internal static readonly HeatProvider Universal = HeatProvider.Instance;

        /// <summary>
        /// Initializes a new instance of the Heat class
        /// </summary>
        public Heat()
        {
            // Set plugin info attributes
            Title = "Heat";
            Author = HeatExtension.AssemblyAuthors;
            Version = HeatExtension.AssemblyVersion;

            CommandManager.OnRegisterCommand += attribute =>
            {
                foreach (string command in attribute.Aliases.InsertItem(attribute.Name, 0))
                {
                    if (Universal.CommandSystem.RegisteredCommands.TryGetValue(command, out HeatCommands.RegisteredCommand universalCommand))
                    {
                        Universal.CommandSystem.RegisteredCommands.Remove(universalCommand.Command);
                        Universal.CommandSystem.RegisterCommand(universalCommand.Command, universalCommand.Source, universalCommand.Callback);
                    }
                }
            };
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();
        }

        #endregion Initialization

        #region Player Hooks

        /// <summary>
        /// Called when the player is banned
        /// </summary>
        /// <param name="evt"></param>
        [Hook("OnPlayerBanned")]
        private void OnPlayerBanned(PlayerKickEvent evt)
        {
            if (evt.Player != null)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerBanned", evt.Player.IPlayer, evt.Reason);
            }
        }

        /// <summary>
        /// Called when the player sends a chat message
        /// </summary>
        /// <param name="evt"></param>
        /// <returns></returns>
        [Hook("IOnPlayerChat")]
        private object IOnPlayerChat(PlayerMessageEvent evt)
        {
            // Ignore the server player
            if (evt.Sender.Equals(CodeHatch.Engine.Networking.Server.ServerPlayer))
            {
                return null;
            }

            switch (evt.Message.Channel)
            {
                //case Channel.Custom:
                //case Channel.Global:
                case Channel.Local:
                    // Is the chat messaged blocked?
                    if (Interface.CallHook("OnPlayerChat", evt.Sender.IPlayer, evt.Message) != null)
                    {
                        // Cancel message event
                        evt.SetCancelled();
                        return true;
                    }
                    break;

                case Channel.Group:
                    // Is the group chat messaged blocked?
                    if (Interface.CallHook("OnGroupChat", evt.Sender.IPlayer, evt.Message) != null) // TODO: Finish implementing
                    {
                        // Cancel message event
                        evt.SetCancelled();
                        return true;
                    }
                    break;

                case Channel.Whisper:
                    // Is the whisper chat messaged blocked?
                    if (Interface.CallHook("OnPlayerWhisper", evt.Sender.IPlayer, evt.Message) != null) // TODO: Finish implementing
                    {
                        // Cancel message event
                        evt.SetCancelled();
                        return true;
                    }
                    break;
            }

            return null;
        }

        /// <summary>
        /// Called when the player runs a command
        /// </summary>
        /// <param name="evt"></param>
        [Hook("IOnPlayerCommand")]
        private object IOnPlayerCommand(PlayerCommandEvent evt)
        {
            // Ignore the server player
            if (evt.Sender.Equals(CodeHatch.Engine.Networking.Server.ServerPlayer))
            {
                return null;
            }

            // Is the player command blocked?
            if (Interface.CallHook("OnPlayerCommand", evt.Sender.IPlayer, evt.Label, evt.CommandArgs) != null)
            {
                // Cancel command event
                evt.SetCancelled();
                return true;
            }

            return null;
        }

        /// <summary>
        /// Called when the player is attempting to connect
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        [Hook("IOnPlayerConnect")]
        private object IOnPlayerConnect(Player player)
        {
            if (player != null)
            {
                Players.PlayerJoin(player.Identifier, player.Name);

                // Call pre hook for plugins
                object loginUniversal = Interface.CallHook("CanClientLogin", player.Name, player.Identifier, player.Connection.IpAddress);
                object loginDeprecated = Interface.CallDeprecatedHook("CanUserLogin", "CanClientLogin", new DateTime(2021, 1, 1), player.Name, player.Identifier, player.Connection.IpAddress);
                object canLogin = loginUniversal is null ? loginDeprecated : loginUniversal;

                // Can the player log in?
                if (canLogin is string || canLogin is bool loginBlocked && !loginBlocked)
                {
                    // Reject the player with message
                    if (CodeHatch.Engine.Networking.Server.PlayerIsQueued(player.ID))
                    {
                        player.ShowPopup("Disconnected", canLogin is string ? canLogin.ToString() : "Connection was rejected"); // TODO: Localization
                        player.Connection.Close(); // TODO: Handle potential NullReferenceException
                    }
                    else
                    {
                        CodeHatch.Engine.Networking.Server.Kick(player, player.ID, player.Name, canLogin is string ? canLogin.ToString() : "Connection was rejected"); // TODO: Localization
                    }
                    return ConnectionError.NoError;
                }

                // Call post hook for plugins
                Interface.CallHook("OnPlayerApproved", player.Name, player.Identifier, player.Connection.IpAddress); // TODO: Handle potential NullReferenceException
                Interface.CallDeprecatedHook("OnUserApproved", "OnPlayerApproved", new DateTime(2021, 1, 1), player.Name, player.Identifier, player.Connection.IpAddress);
            }

            return null;
        }

        /// <summary>
        /// Called when the player has connected
        /// </summary>
        /// <param name="evt"></param>
        /// <returns></returns>
        [Hook("IOnPlayerConnected")]
        private void IOnPlayerConnected(PlayerJoinEvent evt)
        {
            // Ignore the server player
            if (evt.Player.Equals(CodeHatch.Engine.Networking.Server.ServerPlayer)) // TODO: Handle potential NullReferenceException
            {
                return;
            }

            // Add player to default groups
            GroupProvider defaultGroups = Interface.uMod.Auth.Configuration.Groups;
            if (!permission.UserHasGroup(evt.Player.Identifier, defaultGroups.Players))
            {
                permission.AddUserGroup(evt.Player.Identifier, defaultGroups.Players);
            }
            if (evt.Player.HasPermission("admin") && !permission.UserHasGroup(evt.Player.Identifier, defaultGroups.Administrators))
            {
                permission.AddUserGroup(evt.Player.Identifier, defaultGroups.Administrators);
            }

            IPlayer player = Players.FindPlayerById(evt.Player.Identifier);
            if (player != null)
            {
                // Set IPlayer object in Player
                evt.Player.IPlayer = player;

                Players.PlayerConnected(player.Id, player);

                // Call hook for plugins
                Interface.CallHook("OnPlayerConnected", player);
                Interface.CallDeprecatedHook("OnUserConnected", "OnPlayerConnected", new DateTime(2021, 1, 1), player);
            }
        }

        /// <summary>
        /// Called when the player has disconnected
        /// </summary>
        /// <param name="evt"></param>
        [Hook("IOnPlayerDisconnected")]
        private void IOnPlayerDisconnected(PlayerLeaveEvent evt)
        {
            // Ignore the server player
            if (evt.Player.Equals(CodeHatch.Engine.Networking.Server.ServerPlayer)) // TODO: Handle potential NullReferenceException
            {
                return;
            }

            // Call hook for plugins
            Interface.CallHook("OnPlayerDisconnected", evt.Player.IPlayer, lang.GetMessage("Unknown", this, evt.Player.IPlayer.Id)); // TODO: Localization

            Players.PlayerDisconnected(evt.Player.Identifier);
        }

        /// <summary>
        /// Called when the player is kicked
        /// </summary>
        /// <param name="evt"></param>
        [Hook("OnPlayerKicked")]
        private void OnPlayerKicked(PlayerKickEvent evt)
        {
            if (evt.Player != null)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerKicked", evt.Player.IPlayer, evt.Reason);
            }
        }

        /// <summary>
        /// Called when the player is respawning
        /// </summary>
        /// <param name="evt"></param>
        [Hook("OnPlayerRespawn")] // Not being called every time?
        private void OnPlayerRespawn(PlayerRespawnEvent evt)
        {
            if (evt.Player != null)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerRespawn", evt.Player.IPlayer, evt.Position);
            }
        }

        /// <summary>
        /// Called when the player is spawning
        /// </summary>
        /// <param name="evt"></param>
        [Hook("OnPlayerSpawn")]
        private void OnPlayerSpawn(PlayerFirstSpawnEvent evt)
        {
            if (evt.Player != null)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerSpawn", evt.Player.IPlayer);
            }
        }

        /// <summary>
        /// Called when the player has spawned
        /// </summary>
        /// <param name="evt"></param>
        [Hook("OnPlayerSpawned")]
        private void OnPlayerSpawned(PlayerSpawnEvent evt)
        {
            if (evt.Player != null)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerSpawned", evt.Player.IPlayer, evt.Position);
            }
        }

        #endregion Player Hooks

        #region Server Hooks

        /// <summary>
        /// Called when a command is ran on the server
        /// </summary>
        /// <param name="cmdInfo"></param>
        /// <returns></returns>
        [Hook("IOnServerCommand")]
        private object IOnServerCommand(CommandInfo cmdInfo)
        {
            // Is the server command blocked?
            if (Interface.CallHook("OnServerCommand", cmdInfo.Label.ToLower(), cmdInfo.RawArg.Split(' ').ToArray()) != null)
            {
                return true;
            }

            return null;
        }

        #endregion Server Hooks
    }
}
